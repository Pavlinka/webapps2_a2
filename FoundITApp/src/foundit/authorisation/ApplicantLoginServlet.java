package foundit.authorisation;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.Cookie;
 

/**
 * Servlet implementation class ApplicantAuth
 */
@WebServlet("/ApplicantLoginServlet")
public class ApplicantLoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private final String userID = "admin";
    private final String password = "admin";
       
    /**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String user = request.getParameter("applicantUser");
        String pwd = request.getParameter("applicantPassword");
 
        if (userID.equals(user) && password.equals(pwd)) {
            Cookie applicantCookie = new Cookie("applicantUser", user);
            // setting cookie to expiry in 60 mins
            applicantCookie.setMaxAge(60 * 60);
            response.addCookie(applicantCookie);
            response.sendRedirect("ApplicantLoginSuccess.jsp");
        } else {
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/ApplicantLogin.html");
            PrintWriter out = response.getWriter();
            out.println("<font color=red>Please make sure you enter UserID/Pass as \"ApplicantUser : ApplicantPassword\".</font>\n");
            rd.include(request, response);
        }

	}
    
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ApplicantLoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	

}
