<%@ page language="java" contentType="text/html; charset=US-ASCII"
    pageEncoding="US-ASCII"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>FoundITApp - Applicant Login Successful - Session managed by Cookies</title>
</head>

<body>
    <div align="center">
        <br> <br>
 
        <%
            String userName = null;
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("applicantUser"))
                        userName = cookie.getValue();
                }
            }
            if (userName == null)
                response.sendRedirect("ApplicantLogin.html");
        %>
        <h3>
            Hi
            <%=userName%>, Login successful.
        </h3>
        <br>
 
        <form action="ApplicantLogoutServlet" method="post">
            <input type="submit" value="Logout">
        </form>
    </div>
</body>
</html>